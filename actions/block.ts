'use server';

import { RoomServiceClient } from 'livekit-server-sdk';
import { revalidatePath } from 'next/cache';
import { env } from 'node:process';

import { getSelf } from '@/lib/auth-service';
import { blockUser, unblockUser } from '@/lib/block-services';

const roomService = new RoomServiceClient(env.LIVEKIT_API_URL!, env.LIVEKIT_API_KEY!, env.LIVEKIT_API_SECRET!);

export const onBlock = async (id: string) => {
    const self = await getSelf();
    let blockedUser;

    try {
        blockedUser = await blockUser(id);
    } catch {
        //
    }

    try {
        await roomService.removeParticipant(self.id, id);
    } catch {
        //
    }

    revalidatePath(`/u/${self.username}/community`);

    if (blockedUser) {
        revalidatePath(`/${blockedUser.blocked.username}`);
    }

    return blockedUser;
};

export const onUnblock = async (id: string) => {
    const self = await getSelf();
    const unblockedUser = await unblockUser(id);

    revalidatePath(`/u/${self.username}/community`);

    return unblockedUser;
};
