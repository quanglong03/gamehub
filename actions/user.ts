'use server';

import { User } from '@prisma/client';

import { getSelf } from '@/lib/auth-service';
import { db } from '@/lib/db';
import { revalidatePath } from 'next/cache';
import { resetIngress } from '@/actions/ingress';

export const updateUser = async (values: Partial<User>) => {
    const self = await getSelf();
    const validData = {
        bio: values.bio,
    };

    const user = await db.user.update({
        where: { id: self.id },
        data: { ...validData },
    });

    revalidatePath(`/u/${user.username}`);
    revalidatePath(`/${user.username}`);

    return user;
};

export const afterSignOut = async (username: string) => {
    const user = await db.user.findUnique({
        where: { username },
        select: {
            id: true,
        },
    });

    if (!user) {
        return;
    }

    resetIngress(user.id);

    // db.stream.update({
    //     where: {
    //         userId: user.id,
    //     },
    //     data: {
    //         streamKey: null,
    //     },
    // });
};
