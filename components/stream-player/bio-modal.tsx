'use client';

import React, { useState, useTransition, useRef } from 'react';
import { toast } from 'sonner';

import { Dialog, DialogClose, DialogContent, DialogHeader, DialogTitle, DialogTrigger } from '@/components/ui/dialog';
import { Button } from '@/components/ui/button';
import { Textarea } from '@/components/ui/textarea';
import { updateUser } from '@/actions/user';

interface BioModalProps {
    initialValue: string | null;
}

const BioModal: React.FC<BioModalProps> = ({ initialValue }) => {
    const [value, setValue] = useState(initialValue || '');
    const [isPending, startTransition] = useTransition();
    const closeRef = useRef<HTMLButtonElement>(null);

    const onSubmit = (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();

        startTransition(() => {
            updateUser({ bio: value })
                .then(() => {
                    closeRef.current?.click();
                    toast.success('User bio updated');
                })
                .catch(() => toast.error('Something went wrong'));
        });
    };

    return (
        <Dialog>
            <DialogTrigger asChild>
                <Button variant='link' size='sm' className='ml-auto'>
                    Edit
                </Button>
            </DialogTrigger>

            <DialogContent>
                <DialogHeader>
                    <DialogTitle>Edit user bio</DialogTitle>
                </DialogHeader>

                <form onSubmit={onSubmit} className='space-y-4'>
                    <Textarea
                        placeholder='User bio'
                        onChange={(e) => setValue(e.target.value)}
                        value={value}
                        disabled={isPending}
                        className='resize-none'
                    />

                    <div className='flex justify-between'>
                        <DialogClose asChild>
                            <Button type='button' variant='ghost' ref={closeRef}>
                                Cancel
                            </Button>
                        </DialogClose>

                        <Button disabled={isPending} type='submit' variant='primary'>
                            Save
                        </Button>
                    </div>
                </form>
            </DialogContent>
        </Dialog>
    );
};

export default BioModal;
