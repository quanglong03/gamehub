'use client';

import Image from 'next/image';

import {
    DropdownMenu,
    DropdownMenuContent,
    DropdownMenuItem,
    DropdownMenuLabel,
    DropdownMenuSeparator,
    DropdownMenuTrigger,
} from '@/components/ui/dropdown-menu';
import { useUser, useAuth } from '@clerk/nextjs';

import { afterSignOut } from '@/actions/user';
import { Skeleton } from '@/components/ui/skeleton';
import { useRouter } from 'next/navigation';

const Profile = () => {
    const { user } = useUser();
    const auth = useAuth();
    const router = useRouter();

    if (!user || !user.username) {
        return <ProfileSkeleton />;
    }

    const handleSignOut = async () => {
        await auth.signOut();
        afterSignOut(user.username!);
        router.replace('/');
        router.refresh();
    };

    return (
        <div>
            <DropdownMenu>
                <DropdownMenuTrigger>
                    <Image
                        src={user.imageUrl}
                        alt='Avatar'
                        width={30}
                        height={30}
                        className='w-auto h-auto rounded-full'
                    />
                </DropdownMenuTrigger>

                <DropdownMenuContent>
                    <DropdownMenuLabel>My Profile</DropdownMenuLabel>
                    <DropdownMenuSeparator />
                    <DropdownMenuItem onClick={handleSignOut}>Sign out</DropdownMenuItem>
                </DropdownMenuContent>
            </DropdownMenu>
        </div>
    );
};

export const ProfileSkeleton = () => <Skeleton className='w-[32px] h-[32px] rounded-full bg-muted-foreground' />;

export default Profile;
