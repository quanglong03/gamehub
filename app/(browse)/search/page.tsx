import { redirect } from 'next/navigation';
import { Suspense } from 'react';

import Results, { ResultsSkeleton } from '@/app/(browse)/search/_components/results';

interface SearchPageProps {
    searchParams: {
        term?: string;
    };
}

const SearchPage: React.FC<SearchPageProps> = ({ searchParams: { term } }) => {
    if (!term) {
        redirect('/');
    }

    return (
        <div className='h-full p-8 max-w-screen-2xl mx-auto'>
            <Suspense fallback={<ResultsSkeleton />}>
                <Results term={term} />
            </Suspense>
        </div>
    );
};

export default SearchPage;
