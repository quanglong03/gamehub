import ResultCard, { ResultCardSkeleton } from '@/app/(browse)/search/_components/result-card';
import { Skeleton } from '@/components/ui/skeleton';
import { getSearch } from '@/lib/search-service';

interface ResultsProps {
    term?: string;
}

const Results: React.FC<ResultsProps> = async ({ term }) => {
    const data = await getSearch(term);

    return (
        <div>
            <h2 className='text-lg font-semibold mb-4'>Results for term &quot;{term}&quot;</h2>

            {data.length === 0 && (
                <p className='text-muted-foreground text-sm'>No results found. Try searching for something else.</p>
            )}

            <div className='flex flex-col gap-y-4'>
                {data.map((result) => (
                    <ResultCard data={result} key={result.id} />
                ))}
            </div>
        </div>
    );
};

export const ResultsSkeleton = () => (
    <div>
        <Skeleton className='h-8 w-[290px] mb-4' />

        <div className='flex flex-col gap-y-4'>
            <ResultCardSkeleton />
            <ResultCardSkeleton />
            <ResultCardSkeleton />
            <ResultCardSkeleton />
        </div>
    </div>
);

export default Results;
