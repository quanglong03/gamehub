import ResultCard, { ResultCardSkeleton } from '@/app/(browse)/(home)/_components/result-card';
import { Skeleton } from '@/components/ui/skeleton';
import { getStreams } from '@/lib/feed-service';

interface ResultsProps {
    //
}

const Results: React.FC<ResultsProps> = async () => {
    const data = await getStreams();

    return (
        <div>
            <h2 className='text-lg font-semibold mb-4'>Streams we thing you&apos;ll like</h2>

            {data.length === 0 && <div className='text-muted-foreground'>No streams found.</div>}

            <div className='grid grid-cols-1 md:grid-cols lg:grid-cols-3 xl:grid-cols-4 2xl:grid-cols-5 gap-4'>
                {data.map((result) => (
                    <ResultCard key={result.id} data={result} />
                ))}
            </div>
        </div>
    );
};

export const ResultsSkeleton = () => (
    <div>
        <Skeleton className='h-8 w-[290px] mb-4' />

        <div className='grid grid-cols-1 md:grid-cols lg:grid-cols-3 xl:grid-cols-4 2xl:grid-cols-5 gap-4'>
            <ResultCardSkeleton />
            <ResultCardSkeleton />
            <ResultCardSkeleton />
            <ResultCardSkeleton />
        </div>
    </div>
);

export default Results;
