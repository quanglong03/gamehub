'use client';

import { StreamPlayerSkeleton } from '@/components/stream-player';

interface UserLoadingProps {
    //
}

const UserLoading: React.FC<UserLoadingProps> = () => {
    return (
        <div className='h-full'>
            <StreamPlayerSkeleton />
        </div>
    );
};

export default UserLoading;
