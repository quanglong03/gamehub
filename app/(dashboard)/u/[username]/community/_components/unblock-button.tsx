'use client';

import { useTransition } from 'react';
import { toast } from 'sonner';

import { onUnblock } from '@/actions/block';
import { Button } from '@/components/ui/button';

interface UnblockButtonProps {
    userId: string;
}

const UnblockButton: React.FC<UnblockButtonProps> = ({ userId }) => {
    const [isPending, startTransition] = useTransition();

    const onClick = () => {
        startTransition(() => {
            onUnblock(userId)
                .then(({ blocked }) => {
                    toast.success(`User ${blocked.username} unblocked`);
                })
                .catch(() => toast.error('Something went wrong'));
        });
    };

    return (
        <Button disabled={isPending} onClick={onClick} variant='link' size='sm' className='text-blue-500 w-full'>
            Unblock
        </Button>
    );
};

export default UnblockButton;
