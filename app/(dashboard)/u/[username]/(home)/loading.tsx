'use client';

import { StreamPlayerSkeleton } from '@/components/stream-player';

const CreatorLoading: React.FC = () => {
    return (
        <div className='h-full'>
            <StreamPlayerSkeleton />
        </div>
    );
};

export default CreatorLoading;
