'use client';

import { useEffect } from 'react';
import { useMediaQuery } from 'usehooks-ts';

import { cn } from '@/lib/utils';
import useCreatorSidebar from '@/store/use-creator-sidebar';

interface ContainerProps {
    children: React.ReactNode;
}

const Container: React.FC<ContainerProps> = ({ children }) => {
    const matches = useMediaQuery('(max-width: 1024px)');
    const { collapsed, onCollapse, onExpand } = useCreatorSidebar((state) => state);

    useEffect(() => {
        if (matches) {
            onCollapse();
        } else {
            onExpand();
        }
    }, [matches, onCollapse, onExpand]);

    return <div className={cn('flex-1 ml-[70px]', !collapsed && 'lg:ml-60')}>{children}</div>;
};

export default Container;
