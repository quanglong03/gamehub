import { redirect } from 'next/navigation';

import Navbar from '@/app/(dashboard)/u/[username]/_components/navbar';
import Sidebar from '@/app/(dashboard)/u/[username]/_components/sidebar';
import Container from '@/app/(dashboard)/u/[username]/_components/container';
import { getSelfByUsername } from '@/lib/auth-service';

interface CreatorLayoutProps {
    children: React.ReactNode;
    params: {
        username: string;
    };
}

const CreatorLayout: React.FC<CreatorLayoutProps> = async ({ children, params: { username } }) => {
    const self = await getSelfByUsername(username);

    if (!self) {
        return redirect('/');
    }

    return (
        <>
            <Navbar />
            <div className='flex h-full pt-20'>
                <Sidebar />
                <Container>{children}</Container>
            </div>
        </>
    );
};

export default CreatorLayout;
